from collections import OrderedDict


def count_rows(items_dict):
    answer_dict = {}
    for item in items_dict:
        answer_dict[item] = len(items_dict[item])
    return answer_dict


def count_percentage(items_dict, length):
    answer_dict = {}
    for item in items_dict:
        try:
            percents = (len(items_dict[item]) / length) * 100
        except ZeroDivisionError:
            percents = 0
        answer_dict[item] = percents
    return answer_dict


def count_average_price(items_dict):
    length = len(items_dict.values())
    answer_dict = {}
    for item in items_dict:
        cost_sum = 0
        for row in items_dict[item]:
            if row[11].__contains__('N/A'):
                length -= 1
            else:
                cost_sum += float(row[11][1:-1].replace(',', ''))
        answer_dict[item] = cost_sum / len(items_dict[item])
    return answer_dict
