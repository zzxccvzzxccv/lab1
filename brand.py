def group_by_brand(reader, brands):
    brand_dict = {brand.lower(): [] for brand in brands}
    for row in reader:
        brand_dict[row[1].lower()].append(row)
    return brand_dict
